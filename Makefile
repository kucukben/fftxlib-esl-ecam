# Copyright (C) 2001-2016 Quantum ESPRESSO group
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License. See the file `License' in the root directory
# of the present distribution.

include make.inc

default :
	@echo 'Hello! To install this package, type at the shell prompt:'
	@echo '  ./configure [--prefix=]'
	@echo '  make [-j] target'
	@echo 'Choose a target: libfftx, FFTXexamples, clean'

###########################################################
# Main targets
###########################################################

# The syntax "( cd PW ; $(MAKE) TLDEPS= all || exit 1)" below
# guarantees that error code 1 is returned in case of error and make stops
# If "|| exit 1" is not present, the error code from make in subdirectories
# is not returned and make goes on even if compilation has failed

libfftx : libdir 
	 cd src ; $(MAKE) TLDEPS= LIB || exit 1 ; cd ../lib ; ln -fs ../src/libfftx.a . 

FFTXexamples: bindir libfftx 
	 cd src ; $(MAKE) TLDEPS= TEST || exit 1 ;  cd ../examples ; ./run_all_examples

libdir :
	test -d lib || mkdir lib

bindir :
	test -d bin || mkdir bin



#############################################################
# Targets for external libraries
############################################################

#libblas : touch-dummy
#	cd install ; $(MAKE) -f extlibs_makefile $@

#liblapack: touch-dummy
#	cd install ; $(MAKE) -f extlibs_makefile $@


#########################################################
# plugins
#########################################################

touch-dummy :
	$(dummy-variable)

#########################################################
# "make links" produces links to all executables in bin/
#########################################################

# Contains workaround for name conflicts (dos.x and bands.x) with WANT
links : bindir
	( cd bin/ ; \
	rm -f *.x ; \
	for exe in ../*/*/*.x ../*/bin/* ; do \
	    if test ! -L $$exe ; then ln -fs $$exe . ; fi \
	done ; \
	)

#########################################################
# Other targets: clean up
#########################################################

# remove object files and executables
clean :
	touch make.inc ; cd src ; $(MAKE) TLDEPS= clean; cd ../; /bin/rm -rf bin/*.x lib/*.a tmp
#	for dir in \
	    src \
	; do \
	    if test -d $$dir ; then \
		( cd $$dir ; \
		$(MAKE) TLDEPS= clean ) \
	    fi \
	done
#	- @(cd install ; $(MAKE) -f plugins_makefile clean)
#	- @(cd install ; $(MAKE) -f extlibs_makefile clean)
#	- /bin/rm -rf bin/*.x lib/*.a tmp

# remove files produced by "configure" as well
veryclean : clean
	- @(cd install ; $(MAKE) -f plugins_makefile veryclean)
	- @(cd install ; $(MAKE) -f extlibs_makefile veryclean)
	- cd install ; rm -f config.log configure.msg config.status \
		ChangeLog* intel.pcl */intel.pcl
	- rm -rf include/configure.h
	- cd install ; rm -fr autom4te.cache
	- cd install; ./clean.sh ; cd -
	- cd include; ./clean.sh ; cd -
	- rm -f project.tar.gz
	- rm -rf make.inc

# remove everything not in the original distribution
distclean : veryclean
	( cd install ; $(MAKE) -f plugins_makefile $@ || exit 1 )

tar :
	@if test -f eslw_drivers.tar.gz ; then /bin/rm project.tar.gz ; fi
	# do not include unneeded stuff
	find ./ -type f | grep -v -e /.svn/ -e'/\.' -e'\.o$$' -e'\.mod$$'\
		-e /.git/ -e'\.a$$' -e'\.d$$' -e'\.i$$' -e'_tmp\.f90$$' -e'\.x$$' \
		-e'~$$' -e'\./GUI' -e '\./tempdir' | xargs tar rvf eslw_drivers.tar
	gzip eslw_drivers.tar

#########################################################
# Tools for the developers
#########################################################

# NOTICE about "make doc": in order to build the .html and .txt
# documentation in Doc, "tcl", "tcllib", "xsltproc" are needed;
# in order to build the .pdf files in Doc, "pdflatex" is needed;
# in order to build html files for user guide and developer manual,
# "latex2html" and "convert" (from Image-Magick) are needed.
doc : touch-dummy
	if test -d Doc ; then \
	( cd Doc ; $(MAKE) TLDEPS= all ) ; fi
	for dir in */Doc; do \
	( if test -f $$dir/Makefile ; then \
	( cd $$dir; $(MAKE) TLDEPS= all ) ; fi ) ;  done

doc_clean :
	if test -d Doc ; then \
	( cd Doc ; $(MAKE) TLDEPS= clean ) ; fi
	for dir in */Doc; do \
	( if test -f $$dir/Makefile ; then \
	( cd $$dir; $(MAKE) TLDEPS= clean ) ; fi ) ;  done

depend:
	@echo 'Checking dependencies...'
	- ( if test -x install/makedeps.sh ; then install/makedeps.sh ; fi)
# update file containing version number before looking for dependencies
